class Snake {

    constructor() {
        this.body = [];
        this.body[0] = createVector(0, 0);
        this.xdir = 0;
        this.ydir = 0;
        this.len = 0;
    }

    addBody() {
        this.len++;
        let head = this.body[this.body.length - 1].copy();
        this.body.push(head);
    }

    show() {
        fill(0);
        for (let i = 0; i < this.body.length; i++) {
            rect(this.body[i].x, this.body[i].y, 1, 1);
        }
    }

    update() {


        let head = this.body[this.body.length - 1].copy();
        this.body.shift();
        head.x += this.xdir;
        head.y += this.ydir;
        this.body.push(head);

    }

    changeDirection(x, y) {
        this.xdir = x;
        this.ydir = y;
    }

    eat(foodPosition) {
        let x = this.body[this.body.length - 1].x;
        let y = this.body[this.body.length - 1].y;
        if (foodPosition.x == x && foodPosition.y == y) {
            console.log("Food eaten")
            return true;
        } else {
            return false;
        }
    }

    endGame() {
        let x = this.body[this.body.length - 1].x;
        let y = this.body[this.body.length - 1].y;

        if (x < 0 || y < 0 || x > 30 || y > 25) {
            console.log("Score is " + (10 * this.body.length - 1));
            return true;
        }
    }
}