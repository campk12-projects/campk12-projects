let snake
let rez = 10;
let food;
let w;
let h;

function setup() {
    createCanvas(300, 250);
    frameRate(15);
    w = floor(width / rez);
    h = floor(height / rez);
    snake = new Snake();
    createFood();
}

function createFood() {
    let x = floor(random(w));
    let y = floor(random(h));
    food = createVector(x, y);
}

function keyPressed() {
    if (keyCode == LEFT_ARROW) {
        snake.changeDirection(-1, 0)
    }
    if (keyCode == RIGHT_ARROW) {
        snake.changeDirection(1, 0)
    }

    if (keyCode == UP_ARROW) {
        snake.changeDirection(0, -1)
    }

    if (keyCode == DOWN_ARROW) {
        snake.changeDirection(0, 1)
    }
}

function draw() {
    scale(rez);
    background(255);

    if (snake.eat(food)) {
        createFood();
        snake.addBody();
    }

    snake.show();
    snake.update();
    if (snake.endGame()) {
        console.log("Game over");
        noLoop();

        background(255, 0, 0);
    }

    noStroke();
    fill(255, 0, 0);
    rect(food.x, food.y, 1, 1);

}